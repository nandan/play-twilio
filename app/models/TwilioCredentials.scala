package models

/**
 * Created by nandan on 17/05/14.
 */
case class TwilioCredentials(sid: String, token: String, appSID: String)

case class TwSMS(phone: String, msg: String)